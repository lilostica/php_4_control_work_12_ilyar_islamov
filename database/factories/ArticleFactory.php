<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Article;
use Faker\Generator as Faker;

$factory->define(Article::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'body'=> $faker->text($maxNbChars = 300),
        'user_id' => rand(1,11),
        'category_id' => rand(1,4),
        'tag_id' => rand(1,5)
    ];
});
