<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Rating;
use Faker\Generator as Faker;

$factory->define(Rating::class, function (Faker $faker) {
    $happy_unhappy = ['happy','unhappy'];
    return [
        'user_id' => rand(1,11),
        'article_id' => rand(1,30),
        'quality' => rand(-1,1),
        'actual' => rand(-1,1),
        'happy_unhappy' => $happy_unhappy[rand(0,1)]
    ];
});
