<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->insert(
            [
                'title' => 'Football',
            ]
        );

        DB::table('tags')->insert(
            [
                'title' => 'Parliament',
            ]
        );

        DB::table('tags')->insert(
            [
                'title' => 'Die',
            ]
        );

        DB::table('tags')->insert(
            [
                'title' => 'Die',
            ]
        );

        DB::table('tags')->insert(
            [
                'title' => 'Dota2',
            ]
        );
    }
}
