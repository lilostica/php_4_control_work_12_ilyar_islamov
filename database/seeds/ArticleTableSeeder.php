<?php

use Illuminate\Database\Seeder;
use App\Article;
use Illuminate\Support\Facades\DB;

class ArticleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );


        DB::table('articles')->insert(
            [
                'title' => 'title',
                'body' => 'President clinging to power as his legal challenges
                 to the election results crumble around him, mindful that
                 he ought to show Americans what he\'s been doing
                 with the power of government as he spends his days
                 tweeting conspiracy theories about lost or
                 deleted votes in the midst of a pandemic
                 that is coursing through the United States.',
                'publish_date' => \Carbon\Carbon::today(),
                'user_id' => rand(1,11),
                'category_id' => rand(1,4),
                'tag_id' => rand(1,5),
                'created_at' => now(),
                'updated_at' => now()
            ]
        );

        factory(Article::class,20)->create();
    }
}
