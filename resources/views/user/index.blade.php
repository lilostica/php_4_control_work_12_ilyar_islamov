@extends('layouts.app')
@section('content')
    <div style="margin: 50px; text-align: center">
        <h3>Name: {{$user->name}}</h3>
        <h3>Email: {{$user->email}}</h3>
        <h3>Posts count: {{$count_articles}}</h3>
        <h3>Rating: {{number_format($rating,2)}}</h3>

    </div>
    @endsection
