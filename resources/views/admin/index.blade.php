@extends('layouts.app')
@section('content')
    <h1>Admin</h1>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">News body</th>
            <th scope="col">Add publish date</th>

        </tr>
        </thead>
        <tbody>
        @foreach($articles as $article)
        <tr>
            <th scope="row">{{$table_row_number++}}</th>
            <td>{{$article->user->name}}</td>
            <td> <a href="{{route('article.show',$article->id)}}"  class="card-link">{{$article->title}}</a></td>
            <td>
                <form method="post" action="{{route('article-publish',$article->id)}}">
                    @method('put')
                    @csrf
                    <div class="form-group">
                        <label for="publish_date">Publish date</label>
                        <input type="text"  value="yyyy/mm/dd" name="publish_date" class="form-control @error("publish_date") is-invalid @enderror" id="publish_date">
                    </div>
                    @error('publish_date')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <button type="submit" class="btn btn-primary">Set</button>
                </form>
            </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    @endsection
