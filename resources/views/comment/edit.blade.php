@extends('layouts.app')
@section('content')
    <h1 style="margin-top: 20px;">Comment Update</h1>
    <form method="post" action="{{route('comment.update',['comment' => $comment])}}">
        @method('put')
        @csrf
        <input type="hidden" name="article_id" value="{{$comment->article->id}}">
        <div class="form-group">
            <label for="comment">Enter comment</label>
            <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="comment" rows="3">{{$comment->body}}</textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button class="btn-primary" type="submit">Update</button>
    </form>
@endsection
