@extends('layouts.app')
@section('content')
    <h1 style="padding-top: 50px;">Articles</h1>
    <button type="button" class="btn btn-secondary"><a style="color: white;" href="{{route('article.create')}}">Create Article</a></button>
    <div class="row">
    @foreach($articles as $article)
        <div class="card" style="width: 18rem; margin: 20px;">
            <div class="card-body">
                <h5 class="card-title"><a href="{{route('article.show',$article->id)}}">{{$article->title}}</a></h5>
                <h6 class="card-subtitle mb-2 text-muted">{{$article->category->title}}</h6>
                <p class="card-text">{{$article->body}}</p>
                <h6 class="card-subtitle mb-2 text-muted">Publish: {{$article->publish_date}}</h6>
                <div class="d-flex justify-content-between">
                    @can('edit',$article)
                <a href="{{route('article.edit',$article->id)}}"  class="card-link">Edit</a>
                    @endcan
                        @can('destroy',$article)
                <form style="margin-left: 20px;" method="post" action="{{route('article.destroy',$article)}}">
                    @method('delete')
                    @csrf
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
                            @endcan
                </div>
            </div>
        </div>
    @endforeach
    </div>
    <div class="row p-5">
        <div class="col-12 offset-4">
            {{$articles->links()}}
        </div>
    </div>
    </div>
    @endsection
