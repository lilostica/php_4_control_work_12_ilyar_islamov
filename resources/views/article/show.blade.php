@extends('layouts.app')
@section('content')
    <h1 style="margin-top:50px; text-align: center;">{{$article->title}}</h1>
    <div class="jumbotron">
        <h3>Author: {{$article->user->name}}</h3>
        <p class="lead">{{$article->body}}</p>
        <h5>News rating: {{number_format($rating,2)}}</h5>
        <form method="post" action="{{route('add-rating')}}">
            @csrf
            <h4 style="margin-top: 40px;">Send Feedback</h4>
            <input type="hidden" name="user_id" value="{{$article->user->id}}">
            <input type="hidden" name="article_id" value="{{$article->id}}">
        <div class="d-flex justify-content-between">
            <div class="form-group">
                <label for="quality">Quality</label>
                <select name="quality" class="form-control" id="quality">
                    <option value="{{0}}">Select quality</option>
                    <option value="{{1}}">High</option>
                    <option value="{{-1}}">low</option>
                </select>
                <div class="form-group">
                    <label for="actual">Actual</label>
                    <select class="form-control"  name="actual" id="actual">
                        <option value="{{0}}">Select actual</option>
                        <option value="{{1}}">Actual</option>
                        <option value="{{-1}}">Not actual</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="happy_unhappy">how's the news</label>
                    <select class="form-control" name="happy_unhappy" id="happy_unhappy">
                        <option>how's the news</option>
                        <option value="happy">Happy</option>
                        <option value="unhappy">Unhappy</option>
                    </select>
                </div>
            </div>
        </div>
            <button style="margin-bottom: 20px;" class="btn-primary" type="submit">Send</button>
        </form>
    </div>

    <button style="margin-top: 10px;" class="btn btn-primary" type="button" data-toggle="collapse" data-target="#comment" aria-expanded="false" aria-controls="collapseExample">
        Comments
    </button>
    @foreach($article->comments as $comment)
        <div class="collapse comment" id="comment">
            <div style="margin-top: 10px" class="card card-body">
                <h4>{{$comment->user->name}}</h4>
                {{$comment->body}}
                <div class="d-flex justify-content-between">
                    @can('edit',$comment)
                        <a style="width: 100px; margin-top: 5px" class="btn btn-primary" href="{{route('comment.edit',$comment->id)}}">edit</a>
                    @endcan
                @can('delete',$comment)
                    <form  method="post"  action="{{ route('comment.destroy',$comment->id) }}">
                        @method('DELETE')
                        @csrf
                        <button class="like btn-danger" type="submit">delete</button>
                    </form>
                @endcan
                </div>
            </div>
        </div>
    @endforeach
    <form method="post" action="{{route('comment.store')}}">
        @csrf
        <input type="hidden" name="article_id" value="{{$article->id}}">
        <div class="form-group">
            <label for="comment">Enter comment</label>
            <textarea name="body" class="form-control @error('body') is-invalid @enderror" id="comment" rows="3"></textarea>
            @error('body')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <button style="margin-bottom: 20px;" class="btn-primary" type="submit">Send</button>
    </form>
    @endsection
