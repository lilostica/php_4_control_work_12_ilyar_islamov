<?php

namespace App\Policies;

use App\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Comment $comment
     * @return bool
     */
    public function edit(User $user, Comment $comment)
    {
        if ($user->id == $comment->user->id || $user->is_admin == true)
        {
            return true;
        }
        return  false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User  $user
     * @param  \App\Comment  $comment
     * @return mixed
     */
    public function delete(User $user, Comment $comment)
    {
        if ($user->id == $comment->user->id || $user->is_admin == true)
        {
            return true;
        }
        return false;
    }
}
