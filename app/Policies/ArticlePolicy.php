<?php

namespace App\Policies;

use App\Article;
use App\Http\Requests\ArticleRequest;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ArticlePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function edit(User $user, Article $article)
    {
        if ($user->id == $article->user->id || $user->is_admin == true)
        {
            return true;
        }
        return  false;
    }

    /**
     * @param User $user
     * @param Article $article
     * @return bool
     */
    public function destroy(User $user, Article $article)
    {
        if ($user->id == $article->user->id || $user->is_admin == true)
        {
            return true;
        }
        return  false;
    }
}
