<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\CommentRequest;


class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(CommentRequest $request)
    {
        $user = $request->user();
        $comment = new Comment();
        $comment->article_id = $request->input('article_id');
        $comment->user_id = $user->id;
        $comment->body = $request->input('body');
        $comment->save();
        return back()->with('status', 'Comment has added');
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {

        $comment = Comment::findOrFail($id);
        $this->authorize('edit',$comment);
        return view('comment.edit',compact('comment'));
    }



    public function update(CommentRequest $request, $id)
    {
        $comment = Comment::findOrFail($id);
        $user = $request->user();
        $comment->user_id = $user->id;
        $comment->article_id = $request->input('article_id');
        $comment->body = $request->input('body');
        $comment->save();
        return redirect(route('article.show',$comment->article->id))->with('status', 'Comment has updated');

    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($id)
    {
        $comment = Comment::findOrFail($id);
        $this->authorize('delete',$comment);
        $comment->delete();
        return back()->with('status', 'Comment has deleted');
    }
}
