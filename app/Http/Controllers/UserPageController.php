<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index(Request $request)
    {
        $user = User::findOrFail($request->user()->id);
        $count_articles = $user->articles->count();
        $quality = $user->rating->sum('quality');
        $actual = $user->rating->sum('actual');
        $rating = ($count_articles + $quality + $actual) / 3;
        return view('user.index',compact('user','count_articles','rating'));
    }
}
