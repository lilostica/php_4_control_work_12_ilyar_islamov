<?php

namespace App\Http\Controllers;

use App\Article;
use App\Http\Requests\AdminPageRequest;
use Illuminate\Http\Request;

class AdminPageController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->user()->is_admin)
        {
            $table_row_number = 1;
            $articles = Article::all()->where('publish_date',null);
            return view('admin.index',compact('articles','table_row_number'));
        }
        return view('403');
    }

    /**
     * @param AdminPageRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(AdminPageRequest $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->publish_date = $request->input('publish_date');
        $article->save();
        return back();
    }
}
