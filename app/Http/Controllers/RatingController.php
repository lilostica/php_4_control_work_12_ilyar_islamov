<?php

namespace App\Http\Controllers;

use App\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function store(Request $request)
    {
        $rating = new Rating($request->all());;
        $rating->save();
        return back()->with('status', 'Feedback is added');
    }
}
