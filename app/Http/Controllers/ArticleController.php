<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Http\Requests\ArticleRequest;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $articles = Article::where('publish_date','<=',Carbon::today())->paginate(6);
        return view('article.index',compact('articles'));
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::all();
        $tags = Tag::all();
        return view('article.create',compact('categories','tags'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(ArticleRequest $request)
    {
        $article = new Article($request->all());
        $article->user_id = $request->user()->id;
        $article->save();
        return redirect(route('article.index'))->with('status',"{$article->title} has created!");
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $article = Article::findOrfail($id);
        $user = $article->user->id;
        $find_user = User::findorFail($user);
        $count_articles = $find_user->articles->count();
        $quality = $article->rating->sum('quality');
        $actual = $article->rating->sum('actual');
        $rating = ($count_articles + $quality + $actual) / 3;
        return view('article.show',compact('article','rating'));
    }


    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $article = Article::findOrfail($id);
        $this->authorize('edit',$article);
        $categories = Category::all();
        $tags = Tag::all();
        return view('article.edit',compact('article','categories','tags'));
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(ArticleRequest $request, $id)
    {
        $article = Article::findOrfail($id);
        $article->update($request->all());
        return redirect(route('article.index'))->with('status',"{$article->title} has updated!");
    }


    public function destroy($id)
    {
        $article = Article::findOrfail($id);
        $this->authorize('destroy',$article);
        $article->delete();
        return redirect(route('article.index'))->with('status',"{$article->title} has deleted!");
    }
}
