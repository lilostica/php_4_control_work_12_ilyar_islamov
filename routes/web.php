<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','ArticleController@index');

Route::resource('article','ArticleController');
Route::resource('comment', 'CommentController')->except(['index','show','create']);
Route::post('rating','RatingController@store')->name('add-rating');
Route::get('user','UserPageController@index')->name('user-page');
Route::get('admin','AdminPageController@index')->name('admin-page');
Route::put('set-date/{id}','AdminPageController@update')->name('article-publish');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
